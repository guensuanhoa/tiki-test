package com.workhard.splitkeyword

import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.workhard.splitkeyword.base.BaseAdapter

class KeywordAdapter : BaseAdapter<KeywordViewHolder, String>() {
    override fun getViewHolder(parent: ViewGroup, viewType: Int): KeywordViewHolder? {
        return KeywordViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_keyword,
                parent,
                false
            )
        )
    }
}