package com.workhard.splitkeyword

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import android.view.View
import com.workhard.splitkeyword.util.disableFastClick

class MainViewModel : ViewModel() {
    var inputKey = ObservableField<String>("")

    var searchHandler = MutableLiveData<String>()

    fun onSearchButtonClick(btnSearch: View) {
        btnSearch.disableFastClick(500)
        searchHandler.value = inputKey.get()
    }
}