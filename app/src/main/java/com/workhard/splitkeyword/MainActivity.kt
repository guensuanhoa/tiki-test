package com.workhard.splitkeyword

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.workhard.splitkeyword.base.BaseBindingActivity
import com.workhard.splitkeyword.databinding.ActivityMainBinding

class MainActivity : BaseBindingActivity<ActivityMainBinding>() {
    lateinit var adapter: KeywordAdapter

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding?.viewModel = MainViewModel()

        adapter = KeywordAdapter()
        viewDataBinding?.rvKeywordList!!.adapter = adapter
        viewDataBinding?.viewModel?.searchHandler!!.observe(this, Observer {
            it?.let {
                adapter.add(0, it)
                viewDataBinding?.rvKeywordList!!.smoothScrollToPosition(0)
            }
        })
    }
}