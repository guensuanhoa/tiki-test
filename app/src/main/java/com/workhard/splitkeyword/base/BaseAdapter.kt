package com.workhard.splitkeyword.base

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.NO_POSITION
import android.view.View
import android.view.ViewGroup
import java.util.*

abstract class BaseAdapter<VH : BaseAdapter.BaseHolder<*, T>, T>(private var mOnItemClickListener: OnItemClickListener<T>? = null) :
    RecyclerView.Adapter<VH>() {
    protected val itemList = ArrayList<T>()

    protected abstract fun getViewHolder(parent: ViewGroup, viewType: Int): VH?

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val viewHolder = getViewHolder(parent, viewType)
        if (viewHolder != null && mOnItemClickListener != null) {
            viewHolder.mViewDataBinding.root.setOnClickListener { view ->
                val pos = viewHolder.adapterPosition
                if (pos != NO_POSITION) {
                    mOnItemClickListener!!.onItemClick(view, pos, itemList[pos])
                }
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        if (!itemList.isNullOrEmpty()) {
            holder.bindData(itemList[position])
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    private fun addNew(items: List<T>?) {
        items?.let {
            itemList.addAll(it)
            notifyDataSetChanged()
        }
    }

    fun addAll(items: List<T>?) {
        items?.let {
            if (itemList.isEmpty()) {
                addNew(it)
            } else {
                if (!items.isEmpty()) {
                    val positionStart = itemList.size
                    itemList.addAll(items)
                    notifyItemRangeInserted(positionStart, items.size)
                }
            }
        }
    }

    fun add(index: Int, item: T) {
        item?.let {
            itemList.add(index, it)
            notifyItemInserted(index)
        }
    }

    open class BaseHolder<out V : ViewDataBinding, in T>(val mViewDataBinding: V) :
        RecyclerView.ViewHolder(mViewDataBinding.root) {

        open fun bindData(data: T) {}
    }

    interface OnItemClickListener<in T> {
        fun onItemClick(v: View, position: Int, data: T)
    }
}