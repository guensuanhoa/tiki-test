package com.workhard.splitkeyword.util

import android.databinding.BindingAdapter
import android.widget.TextView

@BindingAdapter("keyword")
fun keyword(textView: TextView, keyword: String) {
    textView.text = keyword.formatKeyword()
}