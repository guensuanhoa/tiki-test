package com.workhard.splitkeyword.util

import android.os.Handler
import android.view.View
import java.lang.StringBuilder

fun View.disableFastClick(delayTime: Long = 2000L) {
    this.isEnabled = false
    Handler().postDelayed({
        this.isEnabled = true
    }, delayTime)
}

fun String.formatKeyword(): String {
    var temp = this.trim().replace("\\s+".toRegex(), " ")
    val endIndex = temp.length
    if (endIndex > 10 && temp.indexOf(" ") > -1) {
        var first = temp.substring(0, endIndex / 2)
        var second = temp.substring(endIndex / 2, endIndex)
        val firstIndex = endIndex/2 - first.lastIndexOf(" ") - 1
        val secondIndex = second.indexOf(" ")
        val splitIndex = if (secondIndex == -1 || secondIndex >= firstIndex) {
            endIndex/2 - firstIndex
        } else {
            endIndex/2 + secondIndex
        }

        first = temp.substring(0, splitIndex).trim()
        second = temp.substring(splitIndex, endIndex).trim()

        temp = StringBuilder().append(first).appendln().append(second).toString()
    }
    return temp
}