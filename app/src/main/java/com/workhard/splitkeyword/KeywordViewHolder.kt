package com.workhard.splitkeyword

import com.workhard.splitkeyword.base.BaseAdapter
import com.workhard.splitkeyword.databinding.ItemKeywordBinding

class KeywordViewHolder(itemKeywordBinding: ItemKeywordBinding) :
    BaseAdapter.BaseHolder<ItemKeywordBinding, String>(itemKeywordBinding) {
    override fun bindData(data: String) {
        super.bindData(data)
        mViewDataBinding.keyword = data
    }
}